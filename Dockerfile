FROM python:3.9
RUN apt-get update && apt-get install -y default-mysql-client

WORKDIR /app

COPY . .
RUN pip install --no-cache-dir .

EXPOSE 8000
CMD ["uvicorn", "insights4ci.main:app", "--host", "0.0.0.0", "--port", "8000"]
